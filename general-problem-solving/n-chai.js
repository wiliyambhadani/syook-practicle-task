
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

const question1 = () => {
    return new Promise((resolve, reject) => {
        readline.question('enter total  cups of chai n : ', (n) => {
            resolve(parseInt(n))
        })
    })
}
const question2 = () => {
    return new Promise((resolve, reject) => {
        readline.question('enter k : ', (n) => {
            resolve(parseInt(n))
        })
    })
}
const question3 = () => {
    return new Promise((resolve, reject) => {
        readline.question('enter green tea bags g: ', (n) => {
            resolve(parseInt(n))
        })
    })
}
const question4 = () => {
    return new Promise((resolve, reject) => {
        readline.question('enter black tea bags b : ', (n) => {
            resolve(parseInt(n))
        })
    })
}



const main = async () => {
    let n = await question1()
    let k = await question2()
    let g = await question3()
    let b = await question4()
    readline.close()

    let result = []
    let prev = ""
    for (let i = 0; i < n; i++) {
        if (result.length < 1) {
            if (g > b) {
                prev = "Green"
                result.push(prev)
                g = g - 1
            } else {
                prev = "Black"
                result.push(prev)
                b = b - 1
            }

        } else {
            let isSame = false
            const allEqual = arr => arr.every(v => v === arr[0])
            if (i !== 1) {
                isSame = allEqual(result.slice(-k))  // true
            }
            if (isSame && result.slice(-k).length >= k) {
                if (prev === "Green") {
                    if (b > 0) {
                        prev = "Black"
                        result.push(prev)
                        b = b - 1
                    } else {
                        console.log("not possible")
                        return []
                    }

                } else {
                    if (g > 0) {
                        prev = "Green"
                        result.push(prev)
                        g = g - 1
                    }
                    else {
                        console.log("not possible")
                        return []
                    }

                }
            } else {
                if (g > b) {
                    prev = "Green"
                    result.push(prev)
                    g = g - 1
                } else {
                    prev = "Black"
                    result.push(prev)
                    b = b - 1
                }

            }
        }
    }

    console.log(result)
}

main()
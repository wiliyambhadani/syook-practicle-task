
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

readline.question('Enter positive Number : ', n => {
    readline.close();
    perfectNumber(parseInt(n))
});

const perfectNumber = (n) => {
    let factors = []
    for (let i = 1; i < n; i++) {
        if (n % i == 0) {
            factors.push(i)
        }
    }
    let sum = factors.reduce((a, b) => a + b)
    if (sum == n) {
        console.log("Perfect Number")
    } else if (sum > n) {
        console.log("Abundant")
    } else {
        console.log("Deficient")
    }
}

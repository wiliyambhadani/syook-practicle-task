
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

readline.question('Enter positive Number : ', n => {
    readline.close();
    findTrail(parseInt(n))
});


let trail = 0
const findTrail = (n) => {
    if (n == 1) {
        console.log("trail-->>", trail)
        return trail
    } else if (n % 2 == 0) {
        n = n / 2
        trail += 1
        findTrail(n)
    } else {
        n = 3 * n + 1
        trail += 1
        findTrail(n)
    }
}
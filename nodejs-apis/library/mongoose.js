const mongoose = require('mongoose');

const dbUrl = process.env.MONGODB_URL
const logger = require("../config/logger")

const conectDb = async () => {
    try {
        await mongoose.connect(dbUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        });
        logger.info(`conected with database: ${dbUrl}`)
    } catch (error) {
        logger.error(`failed to conect with database: ${dbUrl}`)
        console.log("error: ", error)
        process.exit(0)
    }
}


module.exports = { conectDb }


//we can use jwt/oauth  for now we are using dummy hardcoded token
const token = "b82b414b-7ec7-4c4d-8df8-adeb32ec6ef0"
const checkToken = async (req, res, next) => {
    if (!req.header('Authorization')) {
        return res.status(400).send({
            message: 'Need authorization token to access this route check your headers',
            error: []
        })
    }

    let clientToken = req.header('Authorization')
    if (clientToken !== token) {
        return res.status(401).send({
            message: ' unauthoried',
            error: []
        })
    }
    next()
}

module.exports = { checkToken }

const express = require('express');
const router = express.Router();
const auth = require("../middleware/auth")
const controller = require("../controller/orders.controller")

router.post("", auth.checkToken, controller.post)
router.patch("/complete", auth.checkToken, controller.patch)
router.get("", auth.checkToken, controller.get)

module.exports = router;

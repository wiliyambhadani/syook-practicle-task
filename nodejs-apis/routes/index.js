const express = require('express');
const items = require("./items.routes")
const customers = require("./customer.routes")
const vehical = require("./vehicals.routes")
const orders = require("./orders.routes")



const router = express.Router();



router.use('/items', items);
router.use('/customers', customers);
router.use('/vehical', vehical);
router.use('/orders', orders);

module.exports = router;
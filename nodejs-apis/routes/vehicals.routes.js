
const express = require('express');
const router = express.Router();
const auth = require("../middleware/auth")
const controller = require("../controller/deliveryVehicals.controller")

router.post("", auth.checkToken, controller.post)
router.patch("", auth.checkToken, controller.patch)
router.get("", auth.checkToken, controller.get)

module.exports = router;

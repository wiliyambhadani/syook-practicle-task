
const Joi = require('joi');
const Items = require("../models/Items")
const logger = require("../config/logger")


const Controller = {}

Controller.post = async (req, res) => {
    try {
        let bodySchema = Joi.object({
            name: Joi.string().required(),
            price: Joi.number().required()
        })
        const validate = bodySchema.validate(req.body);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request',
                error: validate.error.details[0].message
            })
        }

        const items = new Items(req.body)
        let resData = await items.save()
        res.send({
            msg: "OK",
            data: resData
        }).status(200)
    } catch (error) {
        logger.error("error : ", error)
        res.send({
            msg: "Internal server error",
            error: ""
        }).status(500)
    }
}
Controller.patch = async (req, res) => {
    try {
        let bodySchema = Joi.object({
            name: Joi.string().allow(""),
            price: Joi.number()
        })
        let querySchema = Joi.object({
            id: Joi.string().required(),
        })
        let validate = bodySchema.validate(req.body);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request',
                error: validate.error.details[0].message
            })
        }
        validate = querySchema.validate(req.query);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request query params',
                error: validate.error.details[0].message
            })
        }
        let resData = await Items.findOneAndUpdate({ _id: req.query.id }, req.body, { returnOriginal: false })
        if (!resData) {
            return res.status(404).send({
                message: 'no records found',
                error: ""
            })
        }
        res.send({
            msg: "OK",
            data: resData
        }).status(200)
    } catch (error) {
        logger.error("error : ", error)
        res.send({
            msg: "Internal server error",
            error: ""
        }).status(500)
    }
}
Controller.get = async (req, res) => {
    try {
        let querySchema = Joi.object({
            id: Joi.string().allow(),
            index: Joi.number().default(0),
            size: Joi.number().default(10)
        })
        let validate = querySchema.validate(req.query);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request',
                error: validate.error.details[0].message
            })
        }
        let skip = req.query.index ? parseInt(req.query.index * req.query.size) : 0
        let limit = req.query.size ? parseInt(req.query.size) : 10
        let q = {}
        if (req.query.id) {
            q = {
                _id: req.query.id
            }
        }
        let resData = await Items.find(q).skip(skip).limit(limit).sort("-createdAt")
        let totalCount = await Items.count(q)
        if (!resData) {
            return res.status(404).send({
                message: 'no records found',
                error: ""
            })
        }
        res.send({
            msg: "OK",
            totalCount,
            data: resData
        }).status(200)
    } catch (error) {
        logger.error("error : ", error)
        res.send({
            msg: "Internal server error",
            error: ""
        }).status(500)
    }
}

module.exports = Controller

const Joi = require('joi');
const Items = require("../models/Items")
const Vechicals = require("../models/DeliveryVechicals")
const Orders = require("../models/Order")
const logger = require("../config/logger");
const Customer = require('../models/Customer');


const Controller = {}

Controller.post = async (req, res) => {
    try {
        let bodySchema = Joi.object({
            itemId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required(),
            customerId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
        })
        const validate = bodySchema.validate(req.body);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request',
                error: validate.error.details[0].message
            })
        }
        //get item data 
        let itemdata = await Items.findOne({ _id: req.body.itemId })
        if (!itemdata) {
            return res.status(404).send({
                message: 'item not found',
                error: ""
            })
        }
        //get customer data
        let customerData = await Customer.findOne({ _id: req.body.customerId })
        if (!customerData) {
            return res.status(404).send({
                message: 'customer not found please create new one',
                error: ""
            })
        }
        //get vechicl that match customer city and available for delivery

        let vehicalData = await Vechicals.findOne({ city: customerData.city, activeOrdersCount: { "$lt": 2 } })
        if (!vehicalData) {
            return res.status(404).send({
                message: 'currently no vehical available',
                error: ""
            })
        }
        let dataToInsert = {
            customerId: req.body.customerId,
            itemId: req.body.itemId,
            price: itemdata.price,
            deliveryVehicleId: vehicalData._id.toString()
        }
        const orders = new Orders(dataToInsert)
        let resData = await orders.save()
        //update active order count for vehical
        await Vechicals.findOneAndUpdate({ _id: vehicalData._id }, { $inc: { activeOrdersCount: 1 } })
        res.send({
            msg: "OK",
            data: resData
        }).status(200)
    } catch (error) {
        logger.error("error : ", error)
        res.send({
            msg: "Internal server error",
            error: ""
        }).status(500)
    }
}
Controller.patch = async (req, res) => {
    try {
        let querySchema = Joi.object({
            orderId: Joi.number().required(),
        })
        validate = querySchema.validate(req.query);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request query params',
                error: validate.error.details[0].message
            })
        }
        //update order data
        let resData = await Orders.findOneAndUpdate({ orderNumber: req.query.orderId }, { isDelivered: true }, { returnOriginal: false })
        if (!resData) {
            return res.status(404).send({
                message: 'no records found',
                error: ""
            })
        }
        //update vechical data
        await Vechicals.findOneAndUpdate({ _id: resData.deliveryVehicleId }, { $inc: { activeOrdersCount: -1 } })
        res.send({
            msg: "OK",
            data: resData
        }).status(200)
    } catch (error) {
        logger.error("error : ", error)
        res.send({
            msg: "Internal server error",
            error: ""
        }).status(500)
    }
}
Controller.get = async (req, res) => {
    try {
        let querySchema = Joi.object({
            id: Joi.string().allow(""),
            isDelivered: Joi.boolean(),
            index: Joi.number().default(0),
            size: Joi.number().default(10)
        })
        let validate = querySchema.validate(req.query);
        if (validate.error) {
            return res.status(400).send({
                message: 'Invalid request',
                error: validate.error.details[0].message
            })
        }
        let skip = req.query.index ? parseInt(req.query.index * req.query.size) : 0
        let limit = req.query.size ? parseInt(req.query.size) : 10
        let q = {}
        if (req.query.id) {
            q = {
                _id: req.query.id
            }
        }
        if (req.query.isDelivered) {
            q = {
                isDelivered: req.query.isDelivered
            }
        }
        let resData = await Orders.find(q).skip(skip).limit(limit).sort("-createdAt")
        let totalCount = await Orders.count(q)
        if (!resData) {
            return res.status(404).send({
                message: 'no records found',
                error: ""
            })
        }
        res.send({
            msg: "OK",
            totalCount,
            data: resData
        }).status(200)
    } catch (error) {
        logger.error("error : ", error)
        res.send({
            msg: "Internal server error",
            error: ""
        }).status(500)
    }
}

module.exports = Controller
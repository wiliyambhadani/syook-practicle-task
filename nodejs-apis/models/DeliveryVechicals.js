const mongoose = require('mongoose')

const DeliveryVehicalsSchema = mongoose.Schema({

    registrationNumber: {
        type: String,
        required: true,
        unique: true
    },
    vehicleType: {
        type: String,
        required: true,
        enum: ["bike", "truck"]
    },
    city: {
        type: String,
        required: true
    },
    activeOrdersCount: {
        type: Number,
        default: 0,
    },

}, {
    timestamps: true
})



module.exports = mongoose.model("DeliveryVehicals", DeliveryVehicalsSchema)
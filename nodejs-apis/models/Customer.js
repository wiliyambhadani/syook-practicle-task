const mongoose = require('mongoose')

const CustomerSchema = mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    }

}, {
    timestamps: true
})



module.exports = mongoose.model("Customer", CustomerSchema)
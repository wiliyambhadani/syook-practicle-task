const mongoose = require('mongoose')

const ItemsSchema = mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    }

}, {
    timestamps: true
})



module.exports = mongoose.model("Items", ItemsSchema)
const mongoose = require('mongoose')

const OrderSchema = mongoose.Schema({

    orderNumber: {
        type: Number,
    },
    itemId: {
        type: String,
        required: true,
        ref: "Items"
    },
    price: {
        type: Number,
        required: true,
    },
    customerId: {
        type: String,
        required: true,
        ref: "Customer"
    },
    deliveryVehicleId: {
        type: String,
        required: true,
        ref: "DeliveryVehicals"
    },
    isDelivered: {
        type: Boolean,
        default: false
    },


}, {
    timestamps: true
})

const autoIncrementSchema = mongoose.Schema({
    name: String,
    seq: { type: Number, default: 0000 }
});

const AutoIncrement = mongoose.model('AutoIncrement', autoIncrementSchema);

OrderSchema.pre('save', async function (next) {
    let lastCount = await AutoIncrement.findOneAndUpdate(
        { name: 'Orders' },
        { $inc: { seq: 1 } },
        { new: true, upsert: true }
    );
    console.log("lastCount---->>", lastCount)
    this.orderNumber = lastCount.seq
    console.log("this---->>", this)
    next()
})



module.exports = mongoose.model("Orders", OrderSchema)
require('dotenv').config()
const express = require('express')
const cors = require('cors');
const morgan = require('morgan')
const db = require("./library/mongoose")
const logger = require("./config/logger")
const routes = require("./routes")
const app = express()





const startServer = async () => {
    await db.conectDb()
    app.use(cors());
    app.use(express.json());
    app.options('*', cors());
    app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))
    app.get("/ping", (req, res) => {
        res.send({
            msg: "Ok Pong!",
            data: []
        }).status(200)
    })

    app.use(routes)
    //Listen on port 3000
    server = app.listen(process.env.PORT)

    logger.info(`Server is up at localhost: ${process.env.PORT}`)
}

startServer()